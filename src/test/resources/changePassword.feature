Feature: ChangePassword

  Scenario: Change password and verify the login is successfully with new password

    Given la pagina "http://todo.ly/" este abierta
    And yo quiero realizar el login
      | email    | roma@coronado.bo |
      | password | roma.coronado            |
    When yo cambio mi password
      | oldpass | roma.coronado |
      | newpass | cflroma  |
    And yo cierro la sesion
    Then yo deberia ingresar con mis nuevas credenciales a la app web
      | email    | marcelo@coronado.bo |
      | password | cflroma             |
