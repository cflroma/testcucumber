package tarea1_Selenium;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;

public class passwordChangeSelenium {
    ChromeDriver driver;

    //**********************   LLENAR LOS DATOS NECESARIOS    ********************
    String usuario = "roger@coronado.bo";
    String newPassword = "AlfaRomeo3";
    String  password= "coronado.roger";


    @BeforeEach
    public void openBrowser() {
        System.setProperty("webdriver.chrome.driver", "src/test/resources/driver/chromedriver.exe");
        driver = new ChromeDriver();

        // implicit wait --> generico
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
        driver.manage().window().maximize();
        driver.get("http://todo.ly/");
    }

    @AfterEach
    public void closeBrowser() throws InterruptedException {
        Thread.sleep(5000);
        driver.quit();
    }

    /*
     *  los localizadores deben ser unicos
     *  id ---> la mejor opcion
     *  xpath ---> sugerido id dinamicos
     * */

    @Test
    public void verifyTheLoginSuccessfully() {

        this.login(usuario, password);

//         click Ingresa al apartado a cambiar de password
        driver.executeScript("OpenSettingsDialog()");


// set old password  id: TextPwOld
        driver.findElement(By.id("TextPwOld")).sendKeys(password);
        //set new password id:   TextPwNew
        driver.findElement(By.id("TextPwNew")).sendKeys(newPassword);


        // ejecutar el boton OK
        WebElement el = driver.findElement(By.cssSelector("span.ui-button-text"));
        el.click();


        // presiionar logout id: ctl00_HeaderTopControl1_LinkButtonLogout
        driver.findElement(By.id("ctl00_HeaderTopControl1_LinkButtonLogout")).click();

//        driver.findElement(By.xpath("//img[@src='/Images/design/pagelogin.png']")).click();

        this.login(usuario, newPassword);

        // verificacion


        Assertions.assertTrue(driver.findElement(By.id("ctl00_HeaderTopControl1_LinkButtonLogout")).isDisplayed(),
                "ERROR, el usuario no pudo iniciar session");
    }

    public void login(String usuario, String password) {

//         click boton login
        driver.findElement(By.xpath("//img[@src='/Images/design/pagelogin.png']")).click();
        // escribir email
        driver.findElement(By.id("ctl00_MainContent_LoginControl1_TextBoxEmail")).sendKeys(usuario);
        //escribir password
        driver.findElement(By.id("ctl00_MainContent_LoginControl1_TextBoxPassword")).sendKeys(password);
        // click boton login
        driver.findElement(By.id("ctl00_MainContent_LoginControl1_ButtonLogin")).click();

    }

}
