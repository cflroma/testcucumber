package tarea1_Selenium;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;
import java.util.Random;


public class loginSelenium {
    ChromeDriver driver;

    String[] usuario = {"roger","marcelo","coronado","flores"};
    //parametros para generar cadena aleaotoria que se concatenará al usuario del listado proporcionado
    char [] chars = "0123456789abcdefghijklmnopqrstuvwxyz".toCharArray();
    int charsLength;
    Random random;
    StringBuffer buffer;

    @BeforeEach
    public void openBrowser(){
        System.setProperty("webdriver.chrome.driver","src/test/resources/driver/chromedriver.exe");
        driver = new ChromeDriver();

        // implicit wait --> generico
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
        driver.manage().window().maximize();
        driver.get("http://todo.ly/");

        random = new Random();
        buffer = new StringBuffer();
        charsLength = chars.length;

    }

    @AfterEach
    public void closeBrowser() throws InterruptedException {
        Thread.sleep(5000);
        driver.quit();
    }

    /*
     *  los localizadores deben ser unicos
     *  id ---> la mejor opcion
     *  xpath ---> sugerido id dinamicos
     * */

    @Test
    public void verifyTheLoginSuccessfully(){
// genera String aleatorio que se concatenará al nombre del listado
        for (int i=0;i<10;i++){
            buffer.append(chars[random.nextInt(charsLength)]);
        }

        for(String _usuario: usuario) {
            //click boton crear cuenta
            driver.findElement(By.xpath("//img[@src='/Images/design/pagesignup.png']")).click();

            //  llenar nombre  id: ctl00_MainContent_SignupControl1_TextBoxFullName
            driver.findElement(By.id("ctl00_MainContent_SignupControl1_TextBoxFullName")).sendKeys(_usuario);

            // llenar email   id:   ctl00_MainContent_SignupControl1_TextBoxEmail
            driver.findElement(By.id("ctl00_MainContent_SignupControl1_TextBoxEmail")).sendKeys(_usuario+"_._"+buffer.toString()+"@coronado.bo");

            // llenar password id:   ctl00_MainContent_SignupControl1_TextBoxPassword
            driver.findElement(By.id("ctl00_MainContent_SignupControl1_TextBoxPassword")).sendKeys(_usuario);

            //  check terminos  id: ctl00_MainContent_SignupControl1_CheckBoxTerms
            driver.findElement(By.id("ctl00_MainContent_SignupControl1_CheckBoxTerms")).click();

            // presionar signup   id:   ctl00_MainContent_SignupControl1_ButtonSignup
            driver.findElement(By.id("ctl00_MainContent_SignupControl1_ButtonSignup")).click();

            // presiionar logout id: ctl00_HeaderTopControl1_LinkButtonLogout
            driver.findElement(By.id("ctl00_HeaderTopControl1_LinkButtonLogout")).click();

        }
     // verificacion

        Assertions.assertTrue(driver.findElement(By.xpath("//img[@src='/Images/design/pagesignup.png']")).isDisplayed(),
                "ERROR, el usuario no pudo iniciar session");
    }

}
